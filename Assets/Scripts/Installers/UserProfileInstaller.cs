using ExperienceSharing.State;
using Zenject;

namespace ExperienceSharing.Installers
{
    public sealed class UserProfileInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            UserState userState = new UserState();

            Container
                .Bind<UserState>()
                .FromInstance(userState)
                .AsSingle();

            Container
                .Bind<IUserName>()
                .To<IUserName>()
                .FromResolveGetter<UserState>(state => state.UserName)
                .AsSingle();

            Container
                .Bind<IObservableUserName>()
                .To<IObservableUserName>()
                .FromResolveGetter<UserState>(state => state.ObservableUserName)
                .AsSingle();
            
            Container
                .Bind<IUserGender>()
                .To<IUserGender>()
                .FromResolveGetter<UserState>(state => state.UserGender)
                .AsSingle();

            Container
                .Bind<IObservableUserGender>()
                .To<IObservableUserGender>()
                .FromResolveGetter<UserState>(state => state.ObservableUserGender)
                .AsSingle();
            
            Container
                .Bind<IUserEmail>()
                .To<IUserEmail>()
                .FromResolveGetter<UserState>(state => state.UserEmail)
                .AsSingle();

            Container
                .Bind<IObservableUserEmail>()
                .To<IObservableUserEmail>()
                .FromResolveGetter<UserState>(state => state.ObservableUserEmail)
                .AsSingle();
            
            Container
                .Bind<IUserPhone>()
                .To<IUserPhone>()
                .FromResolveGetter<UserState>(state => state.UserPhone)
                .AsSingle();

            Container
                .Bind<IObservableUserPhone>()
                .To<IObservableUserPhone>()
                .FromResolveGetter<UserState>(state => state.ObservableUserPhone)
                .AsSingle();
            
            Container
                .Bind<IUserAvatar>()
                .To<IUserAvatar>()
                .FromResolveGetter<UserState>(state => state.UserAvatar)
                .AsSingle();

            Container
                .Bind<IObservableUserAvatar>()
                .To<IObservableUserAvatar>()
                .FromResolveGetter<UserState>(state => state.ObservableUserAvatar)
                .AsSingle();
            
            Container
                .Bind<IRealAvatar>()
                .To<IRealAvatar>()
                .FromResolveGetter<UserState>(state => state.RealAvatar)
                .AsSingle();
        }
    }
}