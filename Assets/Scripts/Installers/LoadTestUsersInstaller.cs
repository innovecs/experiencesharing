using ExperienceSharing.Commands;
using Zenject;

namespace ExperienceSharing.Installers
{
    public sealed class LoadTestUsersInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container
                .BindInterfacesAndSelfTo<LoadTestUsersCommand>()
                .AsSingle();
            
            Container
                .BindInterfacesAndSelfTo<LoadTextureCommand>()
                .AsSingle();
        }
    }
}