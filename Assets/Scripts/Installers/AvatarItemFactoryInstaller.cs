using ExperienceSharing.BehaviorTrees;
using UnityEngine;
using Zenject;

namespace ExperienceSharing.Installers
{
    public sealed class AvatarItemFactoryInstaller : MonoInstaller
    {
        [SerializeField] private AvatarItemTree m_avatarPrefab = default;
        
        public override void InstallBindings()
        {
            Container
                .BindFactory<AvatarItemTree, AvatarItemTree.Factory>()
                .FromComponentInNewPrefab(m_avatarPrefab)
                .AsSingle();
        }
    }
}