using ExperienceSharing.Models;
using UnityEngine;
using Zenject;

namespace ExperienceSharing.Installers
{
    public sealed class AvatarCollectionInstaller : MonoInstaller
    {
        [SerializeField] private AvatarCollection m_collection = default;
        
        public override void InstallBindings()
        {
            Container
                .BindInterfacesAndSelfTo<AvatarCollection>()
                .FromInstance(m_collection)
                .AsSingle();
        }
    }
}