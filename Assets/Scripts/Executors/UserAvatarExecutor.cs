using ExperienceSharing.State;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using UnityEngine;
using Zenject;

namespace ExperienceSharing
{
    public sealed class UserAvatarExecutor : ObserverReadOnlyStateExecutor<Sprite>
    {
        [Inject] IObservableUserAvatar m_observableUserAvatar = default;

        protected override IReadOnlyStateProperty<Sprite> Target => m_observableUserAvatar;
        
        private void Awake()
        {
            Subscribe(m_observableUserAvatar);
        }
    }
}