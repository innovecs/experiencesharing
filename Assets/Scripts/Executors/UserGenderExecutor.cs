using ExperienceSharing.State;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using Zenject;

namespace ExperienceSharing
{
    public sealed class UserGenderExecutor : ObserverReadOnlyStateExecutor<string>
    {
        [Inject] IObservableUserGender m_observableUserGender = default;

        protected override IReadOnlyStateProperty<string> Target => m_observableUserGender;
        
        private void Awake()
        {
            Subscribe(m_observableUserGender);
        }
    }
}