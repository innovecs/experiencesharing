using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using UnityEngine.SceneManagement;

namespace MatchPoker.Client.Runtime
{
    public sealed class SceneActiveExecutor : BehaviorTreeTargetExecutor<IReadOnlyStateProperty<Scene>>
    {
        protected override IReadOnlyStateProperty<Scene> Target => m_target;

        private readonly IStateProperty<Scene> m_target = new StateProperty<Scene>();

        private void Awake()
        {
            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        private void OnDestroy()
        {
            SceneManager.activeSceneChanged -= OnActiveSceneChanged;
        }

        private void OnActiveSceneChanged(Scene _, Scene current)
        {
            m_target.Value = current;
            Execute();
        }

    }
}