using ExperienceSharing.State;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using Zenject;

namespace ExperienceSharing
{
    public sealed class UserPhoneExecutor : ObserverReadOnlyStateExecutor<string>
    {
        [Inject] IObservableUserPhone m_observableUserPhone = default;
        protected override IReadOnlyStateProperty<string> Target => m_observableUserPhone;
        
        private void Awake()
        {
            Subscribe(m_observableUserPhone);
        }
    }
}