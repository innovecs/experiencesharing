using ExperienceSharing.State;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using Zenject;

namespace ExperienceSharing
{
    public sealed class UserEmailExecutor : ObserverReadOnlyStateExecutor<string>
    {
        [Inject] IObservableUserEmail m_observableUserEmail = default;

        protected override IReadOnlyStateProperty<string> Target => m_observableUserEmail;
        
        private void Awake()
        {
            Subscribe(m_observableUserEmail);
        }
    }
}