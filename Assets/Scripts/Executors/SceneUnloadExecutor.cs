using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using UnityEngine.SceneManagement;

namespace MatchPoker.Client.Runtime
{
    public sealed class SceneUnloadExecutor : BehaviorTreeTargetExecutor<IReadOnlyStateProperty<Scene>>
    {
        protected override IReadOnlyStateProperty<Scene> Target => m_target;

        private readonly IStateProperty<Scene> m_target = new StateProperty<Scene>();

        private void Awake()
        {
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        private void OnDestroy()
        {
            SceneManager.sceneUnloaded -= OnSceneUnloaded;
        }

        private void OnSceneUnloaded(Scene unloaded)
        {
            m_target.Value = unloaded;
            Execute();
        }

    }
}