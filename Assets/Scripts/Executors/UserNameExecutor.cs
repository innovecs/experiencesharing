using ExperienceSharing.State;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using Zenject;

namespace ExperienceSharing
{
    public sealed class UserNameExecutor : ObserverReadOnlyStateExecutor<string>
    {
        [Inject] IObservableUserName m_observableUserName = default;

        protected override IReadOnlyStateProperty<string> Target => m_observableUserName;
        
        private void Awake()
        {
            Subscribe(m_observableUserName);
        }
    }
}