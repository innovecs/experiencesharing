using Innovecs.BehaviorTree;
using Innovecs.State;
using UnityEngine;

namespace Innovecs.Extensions
{
    public static partial class TransformExtensions_Child
    {
        public static INode RemoveAllChildren(this Transform transform)
        {
            return new ActionNode(() => { 
                foreach (Transform child in transform)
                {
                    Object.Destroy(child.gameObject);
                }
            });
        }
        
        public static IReadOnlyStateProperty<bool> HasChildren(this Transform transform)
        {
            return new FuncStateProperty<bool>(() => transform.childCount > 0);
        }
        
        public static IReadOnlyStateProperty<Transform> LastChild(this Transform transform)
        {
            return new FuncStateProperty<Transform>(() => transform.GetChild(transform.childCount - 1));
        }

        public static IReadOnlyStateProperty<Transform> LastChild(this IReadOnlyStateProperty<Transform> transform)
        {
            return new FuncStateProperty<Transform>(() => transform.Value.GetChild(transform.Value.childCount - 1));
        }
        
        public static IReadOnlyStateProperty<GameObject> LastChildGameObject(this Transform transform)
        {
            return new FuncStateProperty<GameObject>(() => transform.GetChild(transform.childCount - 1).gameObject);
        }

        public static IReadOnlyStateProperty<GameObject> LastChildGameObject(this IReadOnlyStateProperty<Transform> transform)
        {
            return new FuncStateProperty<GameObject>(() => transform.Value.GetChild(transform.Value.childCount - 1).gameObject);
        }
        
        public static IReadOnlyStateProperty<Transform> FirstChild(this Transform transform)
        {
            return new FuncStateProperty<Transform>(() => transform.GetChild(0));
        }

        public static IReadOnlyStateProperty<Transform> FirstChild(this IReadOnlyStateProperty<Transform> transform)
        {
            return new FuncStateProperty<Transform>(() => transform.Value.GetChild(0));
        }
        
        public static IReadOnlyStateProperty<GameObject> FirstChildGameObject(this Transform transform)
        {
            return new FuncStateProperty<GameObject>(() => transform.GetChild(0).gameObject);
        }

        public static IReadOnlyStateProperty<GameObject> FirstChildGameObject(this IReadOnlyStateProperty<Transform> transform)
        {
            return new FuncStateProperty<GameObject>(() => transform.Value.GetChild(0).gameObject);
        }
    }
}