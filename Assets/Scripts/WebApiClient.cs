using System.Threading;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace ExperienceSharing
{
    public abstract class WebApiClient
    {
        protected async UniTask<TResponse> Get<TResponse>(string uri)
        {
            Debug.Log($"{nameof(WebApiClient)}: Trying to send {nameof(Get)} to {uri}");
            using UnityWebRequest r = UnityWebRequest.Get(uri);
            await r.SendWebRequest();
            if (!string.IsNullOrEmpty(r.error))
            {
                Debug.LogError(r.error);
            }

            Debug.Log($"{nameof(WebApiClient)}: got response from {uri} {r.downloadHandler.text}");

            return JsonConvert.DeserializeObject<TResponse>(r.downloadHandler.text);
        }

        protected async UniTask<TResponse> Get<TResponse>(string uri, CancellationToken cancellationToken)
        {
            Debug.Log($"{nameof(WebApiClient)}: Trying to send {nameof(Get)} to {uri}");
            using UnityWebRequest r = UnityWebRequest.Get(uri);
            await r.SendWebRequest().ToUniTask(cancellationToken: cancellationToken);
            if (!string.IsNullOrEmpty(r.error))
            {
                Debug.LogError(r.error);
            }

            Debug.Log($"{nameof(WebApiClient)}: got response from {uri} {r.downloadHandler.text}");

            return JsonConvert.DeserializeObject<TResponse>(r.downloadHandler.text);
        }
        
        public static async UniTask<Texture2D> GetTexture(string url)
        {
            using UnityWebRequest r = UnityWebRequestTexture.GetTexture(url);
            await r.SendWebRequest();
                
            if (!string.IsNullOrEmpty(r.error))
            {
                Debug.LogError(r.error);
                return null;
            }
                
            return DownloadHandlerTexture.GetContent(r);
        }
    }
}