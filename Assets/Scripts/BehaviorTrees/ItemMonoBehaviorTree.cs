using ExperienceSharing.Models;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.BehaviorTree.Unity3D.TMPro;
using TMPro;
using UnityEngine;

namespace ExperienceSharing.BehaviorTrees
{
    public class ItemMonoBehaviorTree : MonoBehaviorTree<ItemModel>
    {
        [SerializeField] private TextMeshProUGUI m_id = default;
        [SerializeField] private TextMeshProUGUI m_name = default;
        [SerializeField] private TextMeshProUGUI m_date = default;
        
        public override INode Execute(ItemModel model)
        {
            return new Sequence(
                m_id.Text().Assign(model.Id),
                m_name.Text().Assign(model.Name),
                m_date.Text().Assign(model.Date.ToLongTimeString())
            );
        }
    }
}
