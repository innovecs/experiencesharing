using ExperienceSharing.State;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.BehaviorTree.Unity3D.TMPro;
using TMPro;
using UnityEngine;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class OpenEditNamePopupBehaviorTree : MonoBehaviorTree
    {
        [Inject] IUserName m_userName = default;
        
        [SerializeField] private GameObject m_popup = default;
        [SerializeField] private TMP_InputField m_input = default;
        
        public override INode Execute()
        {
            return new Sequence(
                m_popup.SetActiveAsNode(true),
                m_input.Text().Assign(m_userName)
                );
        }
    }
}