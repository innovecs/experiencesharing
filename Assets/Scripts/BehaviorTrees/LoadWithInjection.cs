using System;
using System.Linq;
using ExperienceSharing.API.Response;
using ExperienceSharing.Commands;
using ExperienceSharing.Models;
using ExperienceSharing.State;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using Innovecs.SceneLoading;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class LoadWithInjection : MonoBehaviorTree
    {
        [Inject] private readonly UserState m_userState = default;
        
        [Inject] private readonly IUserName m_userName = default;
        [Inject] private readonly IUserGender m_userGender = default;
        [Inject] private readonly IUserEmail m_userEmail = default;
        [Inject] private readonly IUserPhone m_userPhone = default;
        [Inject] private readonly IUserAvatar m_userAvatar = default;
        [Inject] private readonly IRealAvatar m_realAvatar = default;
        
        [Inject] private readonly ILoadTestUsersCommand m_loadTestUsersCommand = default;
        [Inject] private readonly ILoadTextureCommand m_loadTextureCommand = default;
        [Inject] private readonly ZenjectSceneLoader m_sceneLoader = default;

        [SerializeField] private GameObject m_preloader;
        
        private const string TopPanel = "TopPanel";
        
        public override INode Execute()
        {
            return new Sequence(
                new LoadSceneAsyncWithInject(TopPanel, LoadSceneMode.Additive, m_sceneLoader,
                    container =>
                    {
                        container
                            .Bind<IObservableUserName>()
                            .To<IObservableUserName>()
                            .FromInstance(m_userState.ObservableUserName)
                            .AsSingle();
                        
                        container
                            .Bind<IObservableUserAvatar>()
                            .To<IObservableUserAvatar>()
                            .FromInstance(m_userState.ObservableUserAvatar)
                            .AsSingle();
                    }),
                
                new Selector(
                    new Sequence(
                        m_preloader.SetActiveAsNode(true),
                        m_loadTestUsersCommand.ExecuteAsNode(out IReadOnlyStateProperty<RandomUsersResponse> response, out IReadOnlyStateProperty<Exception> exception),
                        new FuncBehaviorTree<RandomUsersResponse>(response, SetResponseData),
                        m_preloader.SetActiveAsNode(false)
                    ),
                    new DebugLogExceptionFromState(exception)
                )
            );
        }

        private INode SetResponseData(RandomUsersResponse data)
        {
            UserModel user = data.Results.First();
            
            return new Sequence(
                m_userName.Assign(user.Name.ToString()),
                m_userEmail.Assign(user.Email),
                m_userGender.Assign(user.Gender),
                m_userPhone.Assign(user.Phone),
                new Selector(
                    new Sequence(
                        m_loadTextureCommand.ExecuteAsNode(user.Picture.Large, out IReadOnlyStateProperty<Sprite> textureResponse, out IReadOnlyStateProperty<Exception> textureError),
                        new FuncBehaviorTree<Sprite>(textureResponse, sprite => new Sequence(
                            m_userAvatar.Assign(sprite),
                            m_realAvatar.Assign(sprite)
                        ))
                    ),
                    new DebugLogExceptionFromState(textureError)
                )
            );
        }
    }
}