using ExperienceSharing.State;
using Innovecs.State;
using UnityEngine.SceneManagement;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class SmartSceneDependencyTree : MonoBehaviorTree<IReadOnlyStateProperty<Scene>>
    {
        [Inject] private readonly ISmartScene m_smartScene = default;

        public override INode Execute(IReadOnlyStateProperty<Scene> scene)
        {
            return new BehaviorTreeIf(scene.EqualsTo(m_smartScene))
                .Then(gameObject.scene.Unload());
        }
    }
}