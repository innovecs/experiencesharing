using ExperienceSharing.State;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.SceneLoading;
using UnityEngine.SceneManagement;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class OpenEditAvatarBehaviorTree : MonoBehaviorTree
    {
        [Inject] private readonly UserState m_userState = default;

        [Inject] private readonly ZenjectSceneLoader m_sceneLoader = default;

        private const string Avatar = "Avatar";
        
        public override INode Execute()
        {
            return new Sequence(
                new LoadSceneAsyncWithInject(Avatar, LoadSceneMode.Additive, m_sceneLoader,
                    container =>
                    {
                        container
                            .Bind<IRealAvatar>()
                            .To<IRealAvatar>()
                            .FromInstance(m_userState.RealAvatar)
                            .AsSingle();
                        
                        container
                            .Bind<IUserAvatar>()
                            .To<IUserAvatar>()
                            .FromInstance(m_userState.UserAvatar)
                            .AsSingle();
                        
                        container
                            .Bind<IObservableUserAvatar>()
                            .To<IObservableUserAvatar>()
                            .FromInstance(m_userState.ObservableUserAvatar)
                            .AsSingle();
                    })
            );
        }
    }
}