using ExperienceSharing.State;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class AvatarItemChooseTree : MonoBehaviorTree
    {
        [Inject] private readonly IUserAvatar m_userAvatar = default;
        
        [SerializeField] private Image m_image = default;
        
        public override INode Execute()
        {
            IReadOnlyStateProperty<Sprite> currentSprite = new FuncStateProperty<Sprite>(() => m_image.sprite);
            return m_userAvatar.Assign(currentSprite.Value);
        }
    }
}