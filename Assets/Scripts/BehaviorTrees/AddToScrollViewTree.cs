using System;
using ExperienceSharing.Models;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.BehaviorTree.Unity3D.TMPro;
using TMPro;
using UnityEngine;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class AddToScrollViewTree : MonoBehaviorTree
    {
        [SerializeField] private Transform m_container = default;
        [SerializeField] private ItemMonoBehaviorTree m_prefab = default;
        [SerializeField] private TMP_InputField m_nameText = default;

        public override INode Execute()
        {
            return new FuncBehaviorTree<ItemMonoBehaviorTree>(m_prefab.Instantiate(), Spawn);
        }

        private INode Spawn(ItemMonoBehaviorTree item)
        {
            string id = (m_container.childCount + 1).ToString();
            ItemModel model = new ItemModel(id, m_nameText.text, DateTime.Now);

            return new Sequence(
                item.transform.SetParentAsNode(m_container),
                item.transform.LocalScale().Assign(Vector3.one),
                item.Execute(model),
                m_nameText.Text().Assign(string.Empty)
            );
        }
    }
}
