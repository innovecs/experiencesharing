using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using UnityEngine;
using UnityEngine.UI;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class UserAvatarObserverBehaviorTree : MonoBehaviorTree<IReadOnlyStateProperty<Sprite>>
    {
        [SerializeField] private Image m_image = default;
        
        public override INode Execute(IReadOnlyStateProperty<Sprite> data)
        {
            return m_image.Sprite().Assign(data);
        }
    }
}