using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MatchPoker.Client.Runtime
{
    public sealed class SceneDependencyTree : MonoBehaviorTree<IReadOnlyStateProperty<Scene>>
    {
        [SerializeField] private string m_mainSceneName = default;

        public override INode Execute(IReadOnlyStateProperty<Scene> data)
        {
            return
                new BehaviorTreeIf(data.Name().EqualsTo(m_mainSceneName))
                    .Then(gameObject.scene.Unload());
        }
    }
}
