using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.Extensions;
using UnityEngine;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class RemoveAllBehaviorTree : MonoBehaviorTree
    {
        [SerializeField] private Transform m_container = default;
        
        public override INode Execute()
        {
            return m_container.RemoveAllChildren();

        }
    }
}