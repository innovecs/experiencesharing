using System;
using System.Linq;
using ExperienceSharing.API.Response;
using ExperienceSharing.Commands;
using ExperienceSharing.Models;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.BehaviorTree.Unity3D.TMPro;
using Innovecs.State;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class BasicExampleBehaviorTree : MonoBehaviorTree
    {
        [Inject] private readonly ILoadTestUsersCommand m_loadTestUsersCommand = default;
        [Inject] private readonly ILoadTextureCommand m_loadTextureCommand = default;
        
        [SerializeField] private TextMeshProUGUI m_nameText = default;
        [SerializeField] private TextMeshProUGUI m_genderText = default;
        [SerializeField] private TextMeshProUGUI m_emailText = default;
        [SerializeField] private TextMeshProUGUI m_phoneText = default;
        [SerializeField] private Image m_avatar = default;
        
        public override INode Execute()
        {
            return new Selector(
                new Sequence(
                    m_loadTestUsersCommand.ExecuteAsNode(out IReadOnlyStateProperty<RandomUsersResponse> response, out IReadOnlyStateProperty<Exception> exception),
                    new FuncBehaviorTree<RandomUsersResponse>(response, SetResponseData)
                ),
                new DebugLogExceptionFromState(exception)
            );
        }

        private INode SetResponseData(RandomUsersResponse data)
        {
            UserModel user = data.Results.First();

            return new Sequence(
                m_nameText.Text().Assign(user.Name.ToString()),
                m_genderText.Text().Assign(user.Gender),
                m_emailText.Text().Assign(user.Email),
                m_phoneText.Text().Assign(user.Phone),
                new Selector(
                    new Sequence(
                        m_loadTextureCommand.ExecuteAsNode(user.Picture.Large, out IReadOnlyStateProperty<Sprite> textureResponse, out IReadOnlyStateProperty<Exception> textureError),
                        new FuncBehaviorTree<Sprite>(textureResponse, sprite => m_avatar.Sprite().Assign(sprite))
                    ),
                    new DebugLogExceptionFromState(textureError)
                )
            );
        }
    }
}