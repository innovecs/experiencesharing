using ExperienceSharing.State;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.SceneLoading;
using UnityEngine.SceneManagement;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class OpenEditNameSceneWithInjectBehaviorTree : MonoBehaviorTree
    {
        [Inject] private readonly UserState m_userState = default;

        [Inject] private readonly ZenjectSceneLoader m_sceneLoader = default;

        private const string EditName = "EditName";
        
        public override INode Execute()
        {
            return new Sequence(
                new LoadSceneAsyncWithInject(EditName, LoadSceneMode.Additive, m_sceneLoader,
                    container =>
                    {
                        container
                            .Bind<IUserName>()
                            .To<IUserName>()
                            .FromInstance(m_userState.ObservableUserName)
                            .AsSingle();
                        
                        container
                            .Bind<IObservableUserName>()
                            .To<IObservableUserName>()
                            .FromInstance(m_userState.ObservableUserName)
                            .AsSingle();
                    })
            );
        }
    }
}