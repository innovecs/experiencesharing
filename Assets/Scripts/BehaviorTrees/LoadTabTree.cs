using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class LoadTabTree : MonoBehaviorTree
    {
        [SerializeField] private string m_sceneName = default;
        
        private readonly IStateProperty<Scene> m_scene = new StateProperty<Scene>();

        public override INode Execute()
        {
            return new Sequence(
                new BehaviorTreeIf(m_scene.IsLoaded().IsFalse())
                    .Then(new LoadSceneWithInstanceAsync(m_sceneName, m_scene, LoadSceneMode.Additive)),
                m_scene.SetActive()
            );
        }
    }
}