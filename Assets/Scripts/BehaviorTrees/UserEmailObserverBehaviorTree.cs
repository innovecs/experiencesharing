using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.BehaviorTree.Unity3D.TMPro;
using Innovecs.State;
using TMPro;
using UnityEngine;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class UserEmailObserverBehaviorTree : MonoBehaviorTree<IReadOnlyStateProperty<string>>
    {
        [SerializeField] private TextMeshProUGUI m_emailText = default;
        
        public override INode Execute(IReadOnlyStateProperty<string> data)
        {
            return m_emailText.Text().Assign(data);
        }
    }
}