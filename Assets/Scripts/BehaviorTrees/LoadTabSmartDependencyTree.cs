using ExperienceSharing.State;
using Innovecs.State;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.SceneLoading;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class LoadTabSmartDependencyTree : MonoBehaviorTree
    {
        [Inject] private readonly ZenjectSceneLoader m_zenjectSceneLoader = default;
        
        [SerializeField] private string m_sceneName = default;
        
        private readonly IStateProperty<Scene> m_scene = new StateProperty<Scene>();

        public override INode Execute()
        {
            ISmartScene smartScene = new SmartSceneDependencyState(gameObject.scene);
            
            return new Sequence(
                new BehaviorTreeIf(m_scene.IsLoaded().IsFalse())
                    .Then(new LoadSceneAsyncWithInjectAndInstance(m_sceneName, LoadSceneMode.Additive,
                        m_scene, m_zenjectSceneLoader,
                        container =>
                        {
                            container
                                .Bind<ISmartScene>()
                                .To<ISmartScene>()
                                .FromInstance(smartScene)
                                .AsSingle();

                        })),
                m_scene.SetActive()
            );
        }
    }
}