using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MatchPoker.Client.Runtime
{
    public sealed class ActiveSceneCanvasSortTree : MonoBehaviorTree<IReadOnlyStateProperty<Scene>>
    {
        [SerializeField] private int m_activeSortingOrder = default;
        [SerializeField] private int m_unActiveSortingOrder = default;
        [SerializeField] private Canvas m_canvas = default;

        public override INode Execute(IReadOnlyStateProperty<Scene> data)
        {
            return
                new BehaviorTreeIf(data.EqualsTo(gameObject.scene))
                    .Then(m_canvas.SortingOrder().Assign(m_activeSortingOrder))
                    .Else(m_canvas.SortingOrder().Assign(m_unActiveSortingOrder));
        }
    }
}
