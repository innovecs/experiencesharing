using System.Collections.Generic;
using ExperienceSharing.Models;
using ExperienceSharing.State;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using UnityEngine;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class AvatarBehaviorTree : MonoBehaviorTree<IReadOnlyStateProperty<Sprite>>
    {
        [Inject] private readonly AvatarItemTree.Factory m_factory = default;
        [Inject] private readonly IRealAvatar m_realAvatar = default;
        [Inject] private readonly IAvatarCollection m_avatarCollection = default;
        
        [SerializeField] private Transform m_container = default;
        
        private readonly List<AvatarItemTree> m_items = new List<AvatarItemTree>();
        
        public override INode Execute(IReadOnlyStateProperty<Sprite> data)
        {
            IReadOnlyStateProperty<bool> isReal = new FuncStateProperty<bool>(() => m_realAvatar.Value.name.Equals(data.Value.name));

            INode[] nodes = new INode[m_avatarCollection.Sprites.Length + 2];

            nodes[0] = Clear();

            AvatarItemTree realAvatarItemTree = CreateViewTree();
            nodes[1] = realAvatarItemTree.Execute(new AvatarItemModel(isReal.Value, m_realAvatar.Value));
            
            m_items.Add(realAvatarItemTree);

            int index = 2;
            foreach (Sprite sprite in m_avatarCollection.Sprites)
            {
                IReadOnlyStateProperty<bool> isCurrent = new FuncStateProperty<bool>(() => sprite.name.Equals(data.Value.name));

                AvatarItemTree avatarItemTree = CreateViewTree();
                nodes[index] = avatarItemTree.Execute(new AvatarItemModel(isCurrent.Value, sprite));
                index++;
                
                m_items.Add(avatarItemTree);
            }

            return new Sequence(nodes);
        }
        
        private AvatarItemTree CreateViewTree()
        {
            AvatarItemTree view = m_factory.Create();
            Transform t = view.transform;
            t.SetParent(m_container, true);
            t.localScale = Vector3.one;

            return view;
        }
        
        private INode Clear()
        {
            m_factory.Validate();
            foreach (AvatarItemTree item in m_items)
            {
                Destroy(item.gameObject);
            }
            m_items.Clear();
            return ReturnSuccess.Default;
        }
    }
}