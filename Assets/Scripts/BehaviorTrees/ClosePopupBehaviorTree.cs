using ExperienceSharing.State;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using TMPro;
using UnityEngine;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class ClosePopupBehaviorTree : MonoBehaviorTree
    {
        [Inject] IUserName m_userName = default;
        
        [SerializeField] private GameObject m_popup = default;
        [SerializeField] private TMP_InputField m_input = default;
        
        public override INode Execute()
        {
            return new Sequence(
                m_popup.SetActiveAsNode(false),
                m_userName.Assign(m_input.text)
                );
        }
    }
}