using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class LoadWindowSceneTree : MonoBehaviorTree
    {
        [SerializeField] private string m_sceneName = default;

        public override INode Execute()
        {
            return new LoadSceneAsync(m_sceneName, LoadSceneMode.Additive);
        }
    }
}