using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.BehaviorTree.Unity3D.TMPro;
using Innovecs.State;
using TMPro;
using UnityEngine;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class UserPhoneObserverBehaviorTree : MonoBehaviorTree<IReadOnlyStateProperty<string>>
    {
        [SerializeField] private TextMeshProUGUI m_phoneText = default;
        
        public override INode Execute(IReadOnlyStateProperty<string> data)
        {
            return m_phoneText.Text().Assign(data);
        }
    }
}