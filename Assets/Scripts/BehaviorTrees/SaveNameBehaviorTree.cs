using ExperienceSharing.State;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using TMPro;
using UnityEngine;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class SaveNameBehaviorTree : MonoBehaviorTree
    {
        [Inject] IUserName m_userName = default;
        
        [SerializeField] private TMP_InputField m_input = default;
        
        public override INode Execute()
        {
            return new Sequence(
                m_userName.Assign(m_input.text),
                gameObject.scene.Unload()
            );
        }
    }
}