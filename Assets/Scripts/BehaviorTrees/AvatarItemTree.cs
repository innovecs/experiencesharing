using ExperienceSharing.Models;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class AvatarItemTree : MonoBehaviorTree<AvatarItemModel>
    {
        public sealed class Factory : PlaceholderFactory<AvatarItemTree> { }
        
        [SerializeField] private GameObject m_active = default;
        [SerializeField] private Image m_image = default;
        
        public override INode Execute(AvatarItemModel model)
        {
            return new Sequence(
                m_active.SetActiveAsNode(model.Active),
                m_image.Sprite().Assign(model.Sprite)
            );
        }
    }
}