using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.Extensions;
using UnityEngine;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class RemoveLastBehaviorTree : MonoBehaviorTree
    {
        [SerializeField] private Transform m_container = default;
        
        public override INode Execute()
        {
            return new Sequence(
                m_container.HasChildren().IsTrue(),
                m_container.LastChildGameObject().Destroy()
                );
        }
    }
}