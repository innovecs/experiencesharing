using ExperienceSharing.State;
using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.BehaviorTree.Unity3D.TMPro;
using TMPro;
using UnityEngine;
using Zenject;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class EditNameBehaviorTree : MonoBehaviorTree
    {
        [Inject] IUserName m_userName = default;
        
        [SerializeField] private TMP_InputField m_input = default;
        
        public override INode Execute()
        {
            return m_input.Text().Assign(m_userName);
        }
    }
}