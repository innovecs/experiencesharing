using Innovecs.BehaviorTree;
using Innovecs.BehaviorTree.Unity3D;
using UnityEngine.SceneManagement;

namespace ExperienceSharing.BehaviorTrees
{
    public sealed class OpenEditNameSceneBehaviorTree : MonoBehaviorTree
    {
        private const string EditName = "EditNameWithContract";
        
        public override INode Execute()
        {
            return new Sequence(
                new LoadSceneAsync(EditName, LoadSceneMode.Additive)
            );
        }
    }
}