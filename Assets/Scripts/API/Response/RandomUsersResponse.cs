using System.Collections.Generic;
using ExperienceSharing.Models;
using Newtonsoft.Json;

namespace ExperienceSharing.API.Response
{
    public sealed class RandomUsersResponse
    {
        [JsonProperty("results")]
        public IList<UserModel> Results { get; set; }
    }
}