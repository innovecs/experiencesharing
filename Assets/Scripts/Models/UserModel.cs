using System;
using System.Text;
using Newtonsoft.Json;

namespace ExperienceSharing.Models
{
    [Serializable]
    public sealed class UserModel
    {
        [JsonProperty("gender")] public string Gender { get; set; }
        [JsonProperty("name")] public Name Name { get; set; }
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("phone")] public string Phone { get; set; }
        [JsonProperty("picture")] public Picture Picture { get; set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"{nameof(Gender)}={Gender}");
            builder.AppendLine($"{nameof(Name)}={Name}");
            builder.AppendLine($"{nameof(Email)}={Email}");
            builder.AppendLine($"{nameof(Phone)}={Phone}");
            builder.AppendLine($"{nameof(Picture)}={Picture}");
            builder.AppendLine(base.ToString());

            return builder.ToString();
        }
    }
    
    [Serializable]
    public sealed class Name
    {
        [JsonProperty("title")] public string Title { get; set; }
        [JsonProperty("first")] public string First { get; set; }
        [JsonProperty("last")] public string Last { get; set; }

        public override string ToString()
        {
            return $"{Title} {First} {Last}";
        }
    }

    [Serializable]
    public sealed class Picture
    {
        [JsonProperty("large")] public string Large { get; set; }
        [JsonProperty("medium")] public string Medium { get; set; }
        [JsonProperty("thumbnail")]public string Thumbnail { get; set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"{nameof(Large)}={Large}");
            builder.AppendLine($"{nameof(Medium)}={Medium}");
            builder.AppendLine($"{nameof(Thumbnail)}={Thumbnail}");
            builder.AppendLine(base.ToString());

            return builder.ToString();
        }
    }
}