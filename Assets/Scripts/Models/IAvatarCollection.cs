using UnityEngine;

namespace ExperienceSharing.Models
{
    public interface IAvatarCollection
    {
        public Sprite[] Sprites { get; }
    }
}