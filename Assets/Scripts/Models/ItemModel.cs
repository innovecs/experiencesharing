using System;

namespace ExperienceSharing.Models
{
    public sealed class ItemModel
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public DateTime Date { get; private set; }
        
        public ItemModel(string id, string name, DateTime date)
        {
            Id = id;
            Name = name;
            Date = date;
        }

        public override string ToString()
        {
            return $"{nameof(Id)} : {Id}," +
                   $"{nameof(Name)} : {Name}," +
                   $"{nameof(Date)} : {Date}";
        }
    }
}