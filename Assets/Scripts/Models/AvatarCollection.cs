using UnityEngine;

namespace ExperienceSharing.Models
{
    [CreateAssetMenu(fileName = "AvatarCollection", menuName = "ScriptableObjects/AvatarCollection", order = 1)]
    public sealed class AvatarCollection : ScriptableObject, IAvatarCollection
    {
        [SerializeField] private Sprite[] m_sprites = default;

        public Sprite[] Sprites => m_sprites;
    }
}
