using UnityEngine;

namespace ExperienceSharing.Models
{
    public sealed class AvatarItemModel
    {
        public bool Active { get; private set; }
        public Sprite Sprite { get; private set; }
        
        public AvatarItemModel(bool active, Sprite sprite)
        {
            Active = active;
            Sprite = sprite;
        }

        public override string ToString()
        {
            return $"{nameof(Active)} : {Active}," +
                   $"{nameof(Sprite)} : {Sprite}";
        }
    }
}