using System;
using Innovecs.BehaviorTree.Unity3D;
using Innovecs.State;
using UniRx;

namespace ExperienceSharing.State
{
    public abstract class ObservableStateProperty<T> : IObservableStateProperty<T>
    {
        private T m_value;
        private readonly Subject<Entity> m_propertySubject = new Subject<Entity>();

        IDisposable IObservable<Entity>.Subscribe(IObserver<Entity> observer)
        {
            return m_propertySubject.Subscribe(observer);
        }

        T IReadOnlyStateProperty<T>.Value => m_value;

        T IStateProperty<T>.Value
        {
            get => m_value;
            set
            {
                m_value = value;
                m_propertySubject.OnNext(Entity.Default);
            }
        }
    }
}