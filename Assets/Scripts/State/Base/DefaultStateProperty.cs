using System;
using Innovecs.State;
using Newtonsoft.Json;

namespace ExperienceSharing.State
{
    [Serializable]
    public abstract class DefaultStateProperty<T> : IStateProperty<T>
    {
        private T m_value;

        protected DefaultStateProperty(T val = default)
        {
            m_value = val;
        }

        T IReadOnlyStateProperty<T>.Value => m_value;

        [JsonProperty("Value")]
        T IStateProperty<T>.Value
        {
            get => m_value;
            set => m_value = value;
        }
    }
}