using Innovecs.State;

namespace ExperienceSharing.State
{
    public interface IUserName : IStateProperty<string> { }
}