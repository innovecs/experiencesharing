using Innovecs.State;
using UnityEngine;

namespace ExperienceSharing.State
{
    public interface IRealAvatar : IStateProperty<Sprite> { }
}