using UnityEngine;

namespace ExperienceSharing.State
{
    public sealed class UserState
    {
        #region Name
        private sealed class UserNameState : ObservableStateProperty<string>, IObservableUserName { }
        private readonly UserNameState m_userNameState = new UserNameState();
        public IObservableUserName ObservableUserName => m_userNameState;
        public IUserName UserName => m_userNameState;

        #endregion
        
        #region Gender
        private sealed class UserGenderState : ObservableStateProperty<string>, IObservableUserGender { }
        private readonly UserGenderState m_userGenderState = new UserGenderState();
        public IObservableUserGender ObservableUserGender => m_userGenderState;
        public IUserGender UserGender => m_userGenderState;

        #endregion
        
        #region Email
        private sealed class UserEmailState : ObservableStateProperty<string>, IObservableUserEmail { }
        private readonly UserEmailState m_userEmailState = new UserEmailState();
        public IObservableUserEmail ObservableUserEmail => m_userEmailState;
        public IUserEmail UserEmail => m_userEmailState;

        #endregion
        
        #region Phone
        private sealed class UserPhoneState : ObservableStateProperty<string>, IObservableUserPhone { }
        private readonly UserPhoneState m_userPhoneState = new UserPhoneState();
        public IObservableUserPhone ObservableUserPhone => m_userPhoneState;
        public IUserPhone UserPhone => m_userPhoneState;

        #endregion
        
        #region Avatar
        private sealed class UserAvatarState : ObservableStateProperty<Sprite>, IObservableUserAvatar { }
        private readonly UserAvatarState m_userAvatarState = new UserAvatarState();
        public IObservableUserAvatar ObservableUserAvatar => m_userAvatarState;
        public IUserAvatar UserAvatar => m_userAvatarState;
        
        private sealed class RealAvatarState : DefaultStateProperty<Sprite>, IRealAvatar { }
        private readonly RealAvatarState m_realAvatarState = new RealAvatarState();
        public IRealAvatar RealAvatar => m_realAvatarState;

        #endregion
    }
}