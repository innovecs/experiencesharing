using Innovecs.BehaviorTree.Unity3D;

namespace ExperienceSharing.State
{
    public interface IObservableUserPhone : IObservableStateProperty<string>, IUserPhone { }
}