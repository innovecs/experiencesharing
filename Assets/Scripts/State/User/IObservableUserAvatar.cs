using Innovecs.BehaviorTree.Unity3D;
using UnityEngine;

namespace ExperienceSharing.State
{
    public interface IObservableUserAvatar : IObservableStateProperty<Sprite>, IUserAvatar { }
}