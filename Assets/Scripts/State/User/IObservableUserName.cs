using Innovecs.BehaviorTree.Unity3D;

namespace ExperienceSharing.State
{
    public interface IObservableUserName : IObservableStateProperty<string>, IUserName { }
}