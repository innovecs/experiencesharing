using Innovecs.BehaviorTree.Unity3D;

namespace ExperienceSharing.State
{
    public interface IObservableUserGender : IObservableStateProperty<string>, IUserGender { }
}