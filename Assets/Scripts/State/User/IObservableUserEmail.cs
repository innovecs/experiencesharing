using Innovecs.BehaviorTree.Unity3D;

namespace ExperienceSharing.State
{
    public interface IObservableUserEmail : IObservableStateProperty<string>, IUserEmail { }
}