using Innovecs.State;

namespace ExperienceSharing.State
{
    public interface IUserPhone : IStateProperty<string> { }
}