using Innovecs.State;

namespace ExperienceSharing.State
{
    public interface IUserGender : IStateProperty<string> { }
}