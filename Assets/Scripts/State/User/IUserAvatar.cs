using Innovecs.State;
using UnityEngine;

namespace ExperienceSharing.State
{
    public interface IUserAvatar : IStateProperty<Sprite> { }
}