using Innovecs.State;

namespace ExperienceSharing.State
{
    public interface IUserEmail : IStateProperty<string> { }
}