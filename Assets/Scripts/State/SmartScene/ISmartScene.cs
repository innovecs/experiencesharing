using Innovecs.State;
using UnityEngine.SceneManagement;

namespace ExperienceSharing.State
{
    public interface ISmartScene : IStateProperty<Scene> { }
}