using Innovecs.State;
using UnityEngine.SceneManagement;

namespace ExperienceSharing.State
{
    public sealed class SmartSceneDependencyState : ISmartScene
    {
        private Scene m_value;

        public SmartSceneDependencyState(Scene scene = default)
        {
            m_value = scene;
        }

        Scene IReadOnlyStateProperty<Scene>.Value => m_value;

        Scene IStateProperty<Scene>.Value
        {
            get => m_value;
            set => m_value = value;
        }
    }
}