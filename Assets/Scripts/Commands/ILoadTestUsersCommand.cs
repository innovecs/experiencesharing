using ExperienceSharing.API.Response;
using Innovecs.State;

namespace ExperienceSharing.Commands
{
    public interface ILoadTestUsersCommand : IAsyncCommand<RandomUsersResponse>
    {
    }
}