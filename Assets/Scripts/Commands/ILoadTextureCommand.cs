using Innovecs.State;
using UnityEngine;

namespace ExperienceSharing.Commands
{
    public interface ILoadTextureCommand : IAsyncCommand<string, Sprite>
    {
    }
}