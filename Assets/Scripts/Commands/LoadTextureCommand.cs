using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace ExperienceSharing.Commands
{
    public sealed class LoadTextureCommand : WebApiClient, ILoadTextureCommand
    {
        public async Task<Sprite> Execute(string url)
        {
            Texture2D texture = await GetTexture(url).AsTask();
            return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(texture.width / 2, texture.height / 2));
        }
    }
}