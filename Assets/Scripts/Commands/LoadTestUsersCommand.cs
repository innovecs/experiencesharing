using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using ExperienceSharing.API.Response;

namespace ExperienceSharing.Commands
{
    public sealed class LoadTestUsersCommand : WebApiClient, ILoadTestUsersCommand
    {
        public Task<RandomUsersResponse> Execute()
        {
            string uri = $"https://randomuser.me/api/?inc=gender,name,email,phone,picture";

            return Get<RandomUsersResponse>(uri).AsTask();
        }
    }
}